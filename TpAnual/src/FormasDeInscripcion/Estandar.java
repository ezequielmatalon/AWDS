package FormasDeInscripcion;

import java.util.ArrayList;

import ClasesPrincipales.Partido;
import ClasesPrincipales.Persona;

public class Estandar implements FormaDeInscripcion {

	public void inscribirseA(Partido unPartido, Persona unaPersona) {
		
		
		
		
		if (unPartido.tieneMenosDe10Inscriptos()){
			unPartido.inscribirPersona(unaPersona); //si no hay lugar, busca primero si hay inscriptos condicionales (prioridad mas baja) y luego solidarios (prioridad intermedia)
			
			System.out.println("Inscripcion realizada");
			return;
		}
		else{
				for (Persona inscripto : unPartido.getListadoDeInscriptos()){
					if (inscripto.tieneInscripcionCondicional()){
						unPartido.desinscribirPersona(inscripto);
						unPartido.inscribirPersona(unaPersona);
						System.out.println("Inscripcion realizada");
						return;
				}
			}
				for(Persona inscripto : unPartido.getListadoDeInscriptos()){
					if (inscripto.tieneInscripcionSolidaria()){
						unPartido.desinscribirPersona(inscripto);
						unPartido.inscribirPersona(unaPersona);
						System.out.println("Inscripcion realizada");
						return;
					}
				}
				System.out.println("No se pudo inscribir al partido");
				
	}

}
}

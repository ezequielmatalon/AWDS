package FormasDeInscripcion;

import java.util.stream.Stream;

import ClasesPrincipales.Partido;
import ClasesPrincipales.Persona;

public class Solidaria implements FormaDeInscripcion {

	public void inscribirseA(Partido unPartido, Persona unaPersona) {

		if (unPartido.tieneMenosDe10Inscriptos()) {
			unPartido.inscribirPersona(unaPersona);

			System.out.println("Inscripcion realizada");

		} else {
			List<Persona> inscriptosFiltrados = unPartido.getListadoDeInscriptos().stream().filter(ins->ins.tieneInscripcionCondicional() || ins.tieneInscripcionSolidaria()).collect(Collections.a)
			
			if(!inscriptosFiltrados.)
			{
				
			}
			for (Persona inscripto : unPartido.getListadoDeInscriptos()) {
				if (inscripto.tieneInscripcionCondicional()) {
					unPartido.desinscribirPersona(inscripto);
					unPartido.inscribirPersona(unaPersona);
					System.out.println("Inscripcion realizada");
					return;
				}
			}
		}
		System.out.println("No se pudo inscribir al partido");
	}

}

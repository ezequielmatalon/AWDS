package FormasDeInscripcion;

import ClasesPrincipales.*;

public interface FormaDeInscripcion {

	public void inscribirseA(Partido unPartido, Persona unaPersona);

}

package ClasesPrincipales;
import FormasDeInscripcion.FormaDeInscripcion;


public class Persona {
	
	private String nombre;
	private int edad;
	private FormaDeInscripcion formaDeInscripcion;
	
	public void inscribirseA_UnPartido(Partido unPartido){
		this.getFormaDeInscripcion().inscribirseA(unPartido,this); //aca esta el patron strategy
	}
	
	public String nombreFormaDeInscripcion(){
		return (this.getFormaDeInscripcion().getClass().getSimpleName()); //esto funciona, pero no se si esta bueno preguntar por el nombre de una clase
	}
	
	public boolean tieneInscripcionCondicional(){
		return(this.nombreFormaDeInscripcion().equals("Condicional"));
	}
	
	public boolean tieneInscripcionSolidaria(){
		return(this.nombreFormaDeInscripcion().equals("Solidaria"));
	}
	
	public Persona(String unNombre, int unaEdad, FormaDeInscripcion unaForma){ //redefino el constructor
		this.setNombre(unNombre);
		this.setEdad(unaEdad);
		this.setFormaDeInscripcion(unaForma);
	}
	
	//De aca para bajo son todos setters y getters
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	public FormaDeInscripcion getFormaDeInscripcion() {
		return formaDeInscripcion;
	}
	public void setFormaDeInscripcion(FormaDeInscripcion formaDeInscripcion) {
		this.formaDeInscripcion = formaDeInscripcion;
	}
	
	

	
	
	
}

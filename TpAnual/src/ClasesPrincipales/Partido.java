package ClasesPrincipales;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Partido {

	private Date fecha;
	private String lugar;
	private List<Persona> listadoDeInscriptos;
	private List<Persona> equipo_1;
	private List<Persona> equipo_2;

	public Partido(Date unaFecha, String unLugar) { // redefino el constructor

		fecha = unaFecha;
		lugar = unLugar;

	}

	public boolean tieneMenosDe10Inscriptos() {
		return listadoDeInscriptos.size() < 10;
	}

	public void inscribirPersona(Persona persona) { // siempre se agrega al
													// final de la lista
		listadoDeInscriptos.add(persona);
	}

	public void desinscribirPersona(Persona persona) {
		int posicion = this.getListadoDeInscriptos().indexOf(persona);
		this.getListadoDeInscriptos().remove(posicion);
		// cuando quita una persona de la lista automaticamente comprime la
		// lista (no quedan huecos)
	}

	public int cantidadDeInscriptos() {
		return this.getListadoDeInscriptos().size();
	}

	public void generarEquiposTentativos() {
	}

	// estos dos por ahora no se sabe como se implementan
	public void confirmarEquipos() {
	}

	// De aca para abajo son todos setters y getters

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public List<Persona> getListadoDeInscriptos() {
		if (listadoDeInscriptos == null) {
			listadoDeInscriptos = new ArrayList<Persona>();
		}
		return listadoDeInscriptos;
	}

	public void setListadoDeInscriptos(List<Persona> listadoDeInscriptos) {
		this.listadoDeInscriptos = listadoDeInscriptos;
	}

	public String getLugar() {
		return lugar;
	}

	public void setLugar(String lugar) {
		this.lugar = lugar;
	}

	public List<Persona> getEquipo_1() {
		return equipo_1;
	}

	public void setEquipo_1(List<Persona> equipo_1) {
		this.equipo_1 = equipo_1;
	}

	public List<Persona> getEquipo_2() {
		return equipo_2;
	}

	public void setEquipo_2(List<Persona> equipo_2) {
		this.equipo_2 = equipo_2;
	}

}
